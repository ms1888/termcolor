/*
 * Colored terminal output
 * Copyright (C) 2024  Matija Skala <mskala@gmx.com>
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef TERMCOLOR_HH
#define TERMCOLOR_HH

#include <string>

namespace termcolor
{

enum value {
    black,
    red,
    green,
    yellow,
    blue,
    magenta,
    cyan,
    white,
    custom,
    default_color
};

inline std::string escape(std::string s)
{
    static char* TERM = getenv ( "TERM" );
    if (!TERM)
        return {};
    return "\033[" + s;
}

inline auto fg(value v)
{
    return escape("3" + std::to_string(v) + "m");
}

inline auto bg(value v)
{
    return escape("4" + std::to_string(v) + "m");
}

inline auto bold = escape("1m");
inline auto reset = escape("m");

}

#endif // TERMCOLOR_HH
